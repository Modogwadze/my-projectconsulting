import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroComponent } from './intro/intro.component';
import { HeaderComponent } from './header/header.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ServiceComponent } from './service/service.component';
import { ProjectComponent } from './project/project.component';
import { ContactComponent } from './contact/contact.component';
import { TeamComponent } from './team/team.component';


const routes: Routes = [
  {path: '', redirectTo: '/Home', pathMatch: 'full'},
  { path: 'Home', component:  HeaderComponent },
  { path: 'About', component: AboutUsComponent },
  { path: 'Services', component:  ServiceComponent },
  { path: 'Project', component: ProjectComponent },
  { path: 'Team', component:  TeamComponent },
  { path: 'Contact', component: ContactComponent },
  { path: 'Intro', component: IntroComponent } 
  
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
